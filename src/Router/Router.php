<?php
namespace Router;

class Router
{
    private static $routeconf = null;
    protected function __construct()
    {
        //nothing to do
    }
    
    public static function getInstance($routeconf = null)
    {
        //如果沒有指定$routeconf，則預設會直接讀取routeconf.yml
        if (!self::$routeconf) {
            self::$routeconf = yaml_parse(file_get_contents(BASE_DIR.'/route/'.$routeconf.'.yml'));
        }
        return new self;
    }

    public function execute($uri, $classdi = null, $httpmethod = null)
    {
        //比對routeconf
        //先裁切uri,會得到array("","根uri","子1","子2"......)
        $data = explode("/", $uri);
        unset($data[0]);
        $data = array_values($data);
        //建立restful機制。
        //這個判斷式是為了UnitTest注入作的,主程式不指定httpmethod參數
        if (!$httpmethod) {
            $httpmethod = strtolower(getenv('REQUEST_METHOD'));
        }
        //把httpmethod接到根uri上
        $data[0] = $data[0].'.'.$httpmethod;
        //接下來比對uri是否對應到config，有就執行命令，沒有就回傳錯誤。
        $datacount = count($data);
        $routeconf = self::$routeconf;
        //preparemethoddi是用來裝載注入method參數陣列的變數
        $methoddi = array();
        $test = 0;
        for ($i = 0; $i < $datacount; $i++) {
            
            //當第i段URI有Match到config第i層的KEY
            if (isset($routeconf[$data[$i]])) {
                //routeconf陣列推進一層，接續下一輪判斷。
                $routeconf = $routeconf[$data[$i]];
            } else {
                //偵測param-INT字串
                //這個是偵測是否有Match到URI規則的trigger
                $trigger = null;
                if (preg_match('/^param-INT_/', key($routeconf))) {
                    //就檢查參數是否符合INT規則
                    //有，偵測到Match了，設定trigger=true
                    $trigger = true;
                    //符合INT
                    if (filter_var((int)$data[$i], FILTER_VALIDATE_INT)) {
                        $exp = explode('_', key($routeconf));
                        if (isset($exp[2])) {
                            $patternstring = $exp[2];
                            if (preg_match('/^(.*)$/', $patternstring)) {
                                $pattern = substr($patternstring, 1, strlen($patternstring)-2);
                                if (preg_match($pattern, (int)$data[$i])) {
                                    $trigger = true;
                                    $methoddi[explode('_', key($routeconf))[1]] = (int)$data[$i];
                                } else {
                                    return '{"statuscode":"404","detail":"目標不存在"}';
                                }
                            }
                        }
                        $methoddi[explode('_', key($routeconf))[1]] = (int)$data[$i];
                    } else {
                        //不符合INT
                        return '{"statuscode":"404","detail":"目標不存在"}';
                    }
                }
                //使用正規式檢查'param-STR_'字串
                //如果偵測到param-STR
                if (preg_match('/^param-STR_/', key($routeconf))) {
                    //驗證正規式
                    $exp = explode('_', key($routeconf));
                    if (isset($exp[2])) {
                        $patternstring = $exp[2];
                        if (preg_match('/^(.*)$/', $patternstring)) {
                            $pattern = substr($patternstring, 1, strlen($patternstring)-2);
                            if (preg_match($pattern, (string)$data[$i])) {
                                $trigger = true;
                                $methoddi[explode('_', key($routeconf))[1]] = (string)$data[$i];
                            } else {
                                return '{"statuscode":"404","detail":"目標不存在"}';
                            }
                        }
                    }
                    //有，先設定trigger=true
                    $trigger = true;
                    //符合INT
                    $methoddi[explode('_', key($routeconf))[1]] = (string)$data[$i];
                }
                //沒有偵測到Match，此時trigger會是null，進行return。
                if (!$trigger) {
                    return '{"statuscode":"404","detail":"目標不存在"}';
                }
                //routeconf陣列推進一層來判斷
                $routeconf = $routeconf[key($routeconf)];
            }
        }
        //判斷結束，都沒有return就代表match到了routeconf中定義的URI

        //動態產生物件實例
        //判斷是否有classDI
        if (isset($routeconf['classDI']) && $classdi) {
            //對routeconf['classDI']進行array檢測，是array則進行多參數傳輸模式
            //非array則為單參數模式，但將其轉換成array
            if (!is_array($routeconf['classDI'])) {
                $routeconf['classDI'] = array($routeconf['classDI']);
            }
            //用一個for迴圈來組裝參數陣列
            $count = count($routeconf['classDI']);
            //prepareclassdi就是用來裝載注入class參數陣列的變數
            $prepareclassdi = array();
            $j = 0;
            for ($i = 0; $i < $count; $i++) {
                if (isset($classdi[$routeconf['classDI'][$i]])) {
                    $prepareclassdi[$j++] = $classdi[$routeconf['classDI'][$i]];
                }
            }
            //判斷是否為singleton
            //是singleton
            if (isset($routeconf['singleton'])) {
                //利用call_user_func_array動態調用public static method產生物件實例
                $instance = call_user_func_array(
                    $routeconf['class'].'::'.$routeconf['singleton'],
                    //傳入參數陣列
                    $prepareclassdi
                );
            } else {
                //非singleton
                //利用RflectionClass->newInstanceArgs產生物件實例
                $class = new \ReflectionClass($routeconf['class']);
                //傳入參數陣列
                $instance = $class->newInstanceArgs($prepareclassdi);
            }
        } else {
            //無classDI
            //判斷是否為singleton
            //是singleton
            if (isset($routeconf['singleton'])) {
                $instance = call_user_func($routeconf['class'].'::'.$routeconf['singleton']);
            } else {
                //非singleton
                $instance = (new \ReflectionClass($routeconf['class']))->newInstanceArgs();
            }
        }
        //利用ReflectionMethod動態產生要執行的method
        $method = new \ReflectionMethod($instance, $routeconf['method']);
        /**
         * 調用invoke執行method並回傳結果
         * 檢查是否有methodDI，
         * 有，則用invokeArgs進行多參數傳入
         * 無，則用invoke
         */
        //處理methoddi參數
        if (isset($routeconf['methodDI']) && $methoddi) {
            //非array則為單參數模式，但將其轉換成array
            if (!is_array($routeconf['methodDI'])) {
                $routeconf['methodDI'] = array($routeconf['methodDI']);
            }
            $count = count($routeconf['methodDI']);
            $preparemethoddi = array();
            $j = 0;
            //組裝preparemethoddi
            for ($i = 0; $i < $count; $i++) {
                if (isset($methoddi[$routeconf['methodDI'][$i]])) {
                    $preparemethoddi[$j++] = $methoddi[$routeconf['methodDI'][$i]];
                }
            }
            return $method->invokeArgs($instance, $preparemethoddi);
        } else {
            return $method->invoke($instance);
        }
    }

    public function __destruct()
    {
        self::$routeconf = null;
    }
}
