<?php
namespace Router\Controller;

class TestSingletonController
{
    private $di = null;
    private $di2 = null;
    private function __construct($di = null, $di2 = null)
    {
        if ($di) {
            $this->di = $di;
        }
        if ($di2) {
            $this->di2 = $di2;
        }
    }
    public static function getInstance($di = null, $di2 = null)
    {
        return new self($di, $di2);
    }
    public function test3Action()
    {
        return '3';
    }
    public function test4Action()
    {
        if ($this->di) {
            return $this->di->format("Y-m-d H:i:s");
        } else {
            return "no di";
        }
    }
    public function test6Action()
    {
        if ($this->di && $this->di2) {
            $str1 = $this->di->format("Y-m-d H:i:s");
            $str2 = $this->di2->format("Y-m-d H:i:s");
            return $str1.$str2;
        } else {
            return 'no di';
        }
    }
}
