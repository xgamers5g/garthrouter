<?php
namespace Router\Controller;

class TestController
{
    private $di = null;
    private $di2 = null;
    public function __construct($di = null, $di2 = null)
    {
        if ($di) {
            $this->di = $di;
        }
        if ($di2) {
            $this->di2 = $di2;
        }
    }
    public function test1Action()
    {
        return '1';
    }
    public function test2Action()
    {
        if ($this->di) {
            return $this->di->format("Y-m-d H:i:s");
        } else {
            return 'no di';
        }
    }
    public function test5Action()
    {
        if ($this->di && $this->di2) {
            $str1 = $this->di->format("Y-m-d H:i:s");
            $str2 = $this->di2->format("Y-m-d H:i:s");
            return $str1.$str2;
        } else {
            return 'no di';
        }
    }
    public function test7Action($param = null)
    {
        return $param;
    }
    public function test8Action($param = null)
    {
        return $param;
    }
    public function test9Action($param = null)
    {
        return $param;
    }
    public function test10Action($param1 = null, $param2 = null)
    {
        return $param1.$param2;
    }
    public function test11Action($param1 = null)
    {
        return $param1;
    }
    public function test12Action($param1 = null)
    {
        return $param1;
    }
}
