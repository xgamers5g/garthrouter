<?php
ini_set("display_errors", "On");
error_reporting(E_ALL&~E_NOTICE);

require('init.php');
require(implode(DIRECTORY_SEPARATOR, [BASE_DIR, 'vendor', 'autoload.php']));

//Prepare to DI
// $authcore = \Auth\System\Core::getInstance();
// $authfacade = $authcore->getAuthFacade($authcore);
//DI's array
// $di = array (
//     'authfacade' => $authfacade
// );

//get router's Instance
$garthroute = \Router\Router::getInstance('routeconf');
//execute router
$res = $garthroute->execute($_SERVER['DOCUMENT_URI'], $di);

//output cors header
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');
if (isset(json_decode($res, true)['statuscode'])) {
    http_response_code(json_decode($res, true)['statuscode']);
}
//output router callback
echo $res;
