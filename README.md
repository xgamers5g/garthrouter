-----------------------------------------------------------------------------------
2014.11.26 build by Garth_Wang

這是自由，免費的軟體。

-----------------------------------------------------------------------------------
2014.11.26 11:40 改動

開發進度 -完成支援pcre正規式檢查-

-----------------------------------------------------------------------------------
[使用說明]
-----------------------------------------------------------------------------------

    -設定檔

        放置處 /route/routeconf.yml
        使用yaml格式

-----------------------------------------------------------------------------------

    -執行三步驟

        1.[取得物件實體]

                $route = \Router\Router::getInstance();
                可以動態指定要使用的設定
                $route = \Router\Router::getInstance('test');
                此設定將會讀取/route/test.yml

        2.[執行Router，取得回傳值，以nginx為例]

                $result = $route->execute($_SERVER['DOCUMENT_URI'], $classdi);
                參數1: 要導入的URI
                參數2(可選): 要注入的參數陣列

        3.[把結果echo出來，收工]

                echo $result;

-----------------------------------------------------------------------------------

    -HttpMethod設定

        假設URI為 "test"，httpmethod為"post"，請在設定檔中指定「根」URI為 "test.post" ，以(dot)作為分隔符號。

        設置範例：
            ---
            test.post: {
                class: 類別全名,
                method: 方法全名
            }

-----------------------------------------------------------------------------------

    -Singleton(呼叫單例模式的class)設定

        要將URI指定執行Singleton Class，需要指定singleton欄位為getInstance的Method名稱。

        設置範例：
            ---
            test.post: {
                class: 類別全名,
                method: 方法全名,
                singleton: getInstance  //取回物件實體的方法
            }

-----------------------------------------------------------------------------------

    -對建構式傳入參數兩步驟(singleton的getInstance也適用，同時參數支援任意型別)。

        1.[對Router進行參數傳入]

            假設要注入的參數名稱叫做$param，其值為1：

            $route = \Router\Router::getInstance();
            $classdi = array();
            $classdi['param'] = 1;
            $route->execute($_SERVER['DOCUMENT_URI'], $classdi);

        2.[設定檔設置classDI]
            ---
            test.post: {
                class: 類別全名,
                classDI: param1,
                method: 方法全名
            }

        [可以傳入多參數]
            ---
            test.post: {
                class: 類別全名,
                classDI: [param1, param2, param3 .....],
                method: 方法全名
            }

-----------------------------------------------------------------------------------

    -使用URI對Method傳入參數

        支援INT、STR兩種型態的參數傳入:

            param-INT_參數名稱
            param-STR_參數名稱

        設置範例:
            #對照到 /test/(int)
            ---
            test.post: {
                param-INT_param1 : {
                    class: 類別全名,
                    method: 方法全名,
                    methodDI: param1
                }
            }

            #多參數範例 /test/(int)/(str)
            ---
            test.post: {
                param-INT_param1: {
                    param-STR_param2: {
                        class: 類別全名，
                        method: 方法全名,
                        methodDI: [param1, param2]
                    }
                }
            }

-----------------------------------------------------------------------------------

    -使用正規式驗證Method傳入參數

        支援任意型式的正規式驗證:

            param-INT_參數名稱_(preg pattern)
            param-STR_參數名稱_(preg pattern)

        設置範例:
            #對照到 /test/(str) ，正規式pattern為 /^[A-Za-z0-9]+$/
            ---
            test.post: {
                param-STR_param1_(/^[A-Za-z0-9]+$/): {
                    class: 類別全名,
                    method: 方法全名,
                    methodDI: param1
                }
            }

-----------------------------------------------------------------------------------

    -功能組合範例

        假定URI為 : /test/(str)/path1/(int)/(int)/
        httpmethod為 : post
        class名稱為 : TestController.
        singleton構造method為 : getInstance.
        class需要注入的參數為兩個物件。.
        要執行的method名稱為 : testAction
        要對執行method傳入的參數為uri中的三個變數.
        第二個變數需要正規式(/^[A-Za-z0-9]+$/驗證

        [設置classDI]

            $route = \Router\Router::getInstance();
            $classdi = array();
            $obj1 = new class1;
            $obj2 = new class2;
            $classdi['obj1'] = $obj1;
            $classdi['obj2'] = $obj2;
            $route->execute($_SERVER['DOCUMENT_URI'], $classdi);

        [設置routeconf.yml]
            ---
            test.post: {
                param-STR_param1_((/^[A-Za-z0-9]+$/): {
                    path1: {
                        param-INT_param2: {
                            param-INT_param3: {
                                class: TestController,
                                classDI: [obj1, obj2],
                                singleton: getInstance,
                                method: testAction,
                                methodDI: [param1, param2, param3]
                            }
                        }
                    }
                }
            }

            這樣設定的就完成了。

-----------------------------------------------------------------------------------
[NGINX 建議設定]
-----------------------------------------------------------------------------------

        server {
                server_name YOUR_SERVER_DOMAIN;
                listen 80;
                #listen 443 ssl;
                #ssl on;
                #ssl_certificate YOUR_PUBLIC_KEY;
                #ssl_certificate_key YOUR_PRIVATE_KEY;
                root YOUR_SITE_ROOT/www;
                location / {
                    try_files $uri @php;
                }
                location @php {
                    fastcgi_split_path_info ^(.+)(\?/.+)$;
                    fastcgi_pass 127.0.0.1:9000;
                    include fastcgi_params;
                    fastcgi_param SCRIPT_FILENAME $document_root/index.php;
                }
            }
