<?php
namespace Router\UnitTest;

use PHPUnit_Framework_TestCase;

class RouterUnitTest extends PHPUnit_Framework_TestCase
{
    public function testgetInstance()
    {
        $res = \Router\Router::getInstance('test');
        $this->assertEquals(true, $res instanceof \Router\Router);
    }
    public function testexecute()
    {
        $router = \Router\Router::getInstance('test');
        //param1 is URI, param2 is DI, param3 is HttpMethodForTest
        $res = $router->execute('/test/test1', null, 'get');
        $this->assertEquals('1', $res);
    }
    public function testexecuteWithDi()
    {
        $testdi = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $testdi
            ->expects($this->any())
            ->method('format')
            ->will($this->returnValue('123'));
        $router = \Router\Router::getInstance('test');
        $di = array(
            'testdi' => $testdi
        );
        $res = $router->execute('/test/test2', $di, 'get');
        $this->assertEquals('123', $res);
    }
    public function testexecuteWithSingleton()
    {
        $router = \Router\Router::getInstance('test');
        //param1 is URI, param2 is DI, param3 is HttpMethodForTest
        $res = $router->execute('/test/test3', null, 'get');
        $this->assertEquals('3', $res);
    }
    public function testexecuteWithSingletonWithDi()
    {
        $testdi = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $testdi
            ->expects($this->any())
            ->method('format')
            ->will($this->returnValue('456'));
        $router = \Router\Router::getInstance('test');
        $di = array(
            'testdi' => $testdi
        );
        $res = $router->execute('/test/test4', $di, 'get');
        $this->assertEquals('456', $res);
    }
    public function testexecuteWithMultiParamClassDi()
    {
        $testdi1 = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $testdi1
            ->expects($this->any())
            ->method('format')
            ->will($this->returnValue('456'));
        $testdi2 = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $testdi2
            ->expects($this->any())
            ->method('format')
            ->will($this->returnValue('789'));
        $router = \Router\Router::getInstance('test');
        $di = array(
            'testdi1' => $testdi1,
            'testdi2' => $testdi2
        );
        $res = $router->execute('/test/test5', $di, 'get');
        $this->assertEquals('456789', $res);
    }
    public function testexecuteWithSingletonWithMultiParamClassDi()
    {
        $testdi1 = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $testdi1
            ->expects($this->any())
            ->method('format')
            ->will($this->returnValue('456'));
        $testdi2 = $this->getMock('\DateTime', array(), array(), '', false, true, true);
        $testdi2
            ->expects($this->any())
            ->method('format')
            ->will($this->returnValue('789'));
        $router = \Router\Router::getInstance('test');
        $di = array(
            'testdi1' => $testdi1,
            'testdi2' => $testdi2
        );
        $res = $router->execute('/test/test6', $di, 'get');
        $this->assertEquals('456789', $res);
    }
    public function testexecuteWithParamINT()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test7/123', null, 'get');
        $this->assertEquals('123', $res);
    }
    public function testexecuteWithParamINTWithMorePath()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test8/456/test', null, 'get');
        $this->assertEquals('456', $res);
    }
    public function testexecuteWithParamSTR()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test9/asdf', null, 'get');
        $this->assertEquals('asdf', $res);
    }
    public function testexecuteWithMultiParam()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test10/123/456', null, 'get');
        $this->assertEquals('123456', $res);
    }
    public function testexecuteWithParamSTRWithPregIsMatch()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test11/nanaasseeekk99888', null, 'get');
        $this->assertEquals('nanaasseeekk99888', $res);
    }
    public function testexecuteWithParamSTRWithPregNotMatch()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test11/----', null, 'get');
        $this->assertEquals('{"statuscode":"404","detail":"目標不存在"}', $res);
    }
    public function testexecuteWithParamINTWithPregIsMatch()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test12/99888', null, 'get');
        $this->assertEquals(99888, $res);
    }
    public function testexecuteWithParamINTWithPregNotMatch()
    {
        $router = \Router\Router::getInstance('test');
        $res = $router->execute('/test/test12/12345', null, 'get');
        $this->assertEquals('{"statuscode":"404","detail":"目標不存在"}', $res);
    }
}
